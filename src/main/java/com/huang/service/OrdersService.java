package com.huang.service;

import com.huang.pojo.Orders;

import java.util.List;

public interface OrdersService {

    int createOrders(Orders orders);

    Orders getOrdersById(Integer orderId);

    List<Orders> listOrdersByUserId(String userId);

}
