package com.huang.service;

import com.huang.pojo.Food;

import java.util.List;

public interface FoodService {
    List<Food> listFoodByBusinessId(Integer businessId);
}
