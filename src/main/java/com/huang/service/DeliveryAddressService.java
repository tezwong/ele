package com.huang.service;

import com.huang.pojo.DeliveryAddress;

import java.util.List;

public interface DeliveryAddressService {

    //1,按userId查找所有的递送地址
    List<DeliveryAddress> listDeliveryAddressByUserId(String userId);

    //2.按主键获取配送地址
    DeliveryAddress getDeliveryAddressById(Integer daId);

    //3,保存
    int saveDeliveryAddress(DeliveryAddress deliveryAddress);

    //4，修改
    int updateDeliveryAddress(DeliveryAddress deliveryAddress);

    //5,删除
    int removeDeliveryAddress(Integer daId);

}
