package com.huang.service;

import com.huang.pojo.Business;

import java.util.List;

/**
 * @author HuangZiy
 * @date 2020/09/15
 */
public interface BusinessService {
    //按类型找出不同的商家的集合
    List<Business> listBusinessByOrderTypeId(Integer orderTypeId);
    //按商家的ID，获取单体商家
    Business getBusinessById(Integer businessId);
}
