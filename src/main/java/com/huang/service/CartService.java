package com.huang.service;

import com.huang.pojo.Cart;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CartService {
    List<Cart> listCart(Cart cart);
    int saveCart(Cart cart);
    int updateCart(Cart cart);
    int removeCart(Cart cart);
}
