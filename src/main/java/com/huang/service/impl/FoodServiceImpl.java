package com.huang.service.impl;

import com.huang.mapper.FoodMapper;
import com.huang.pojo.Food;
import com.huang.service.FoodService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FoodServiceImpl implements FoodService {

    @Resource
    private FoodMapper foodMapper;

    @Override
    public List<Food> listFoodByBusinessId(Integer businessId) {
        return foodMapper.getFoodByBusinessId(businessId);
    }
}
