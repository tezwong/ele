package com.huang.service;

import com.huang.pojo.User;

public interface UserService {

    User getUserByIdByPwd(User user);

    int getUserById(String userId);

    int saveUser(User user);

}
