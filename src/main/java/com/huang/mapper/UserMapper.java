package com.huang.mapper;

import com.huang.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author HuangZiy
 */
@Mapper
public interface UserMapper {
    /**
     *  用ID和密码登录
     * @param user 传入一个用户对象
     * @return 用户
     */
    @Select("select * from user where userId=#{userId} and password=#{password}")
    User getUserByIdByPwd(User user);

    /**
     *  按ID统计个数
     * @param userId 用户ID
     * @return int型 0或1
     */
    @Select("select count(*) from user where userId=#{userId}")
    int getUserById(String userId);

    /**
     * 注册用户
     * @param user 注册的用户信息
     * @return 整型 0或1
     */
    @Insert("insert into user values(#{userId},#{password},#{userName},#{userSex},null,1)")
    public int saveUser(User user);
}
