package com.huang.mapper;

import com.huang.pojo.Business;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BusinessMapper {

    /**
     * 按不同的点餐分类返回商家集合
     * @param orderTypeId 点餐分类的ID
     * @return 商家集合
     */
    @Select("select * from business where orderTypeId=#{orderTypeId} order by businessId")
    List<Business> listBusinessByOrderTypeId(Integer orderTypeId);

    /**
     * 单体查询，按商家ID查询商家
     * @param businessId 商家ID
     * @return 商家
     */
    @Select("select * from business where businessId=#{businessId}")
    public Business getBusinessById(Integer businessId);


}
