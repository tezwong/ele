package com.huang.mapper;

import com.huang.pojo.Business;
import com.huang.pojo.Food;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface FoodMapper {

    /**
     * 按商家ID来查询,按foodId来排序,在cartMapper.xml中被一对一引用了
     *
     * @param businessId 食品Id;
     * @return 食品集合
     */
    @Select("select * from food where businessId = #{businessId} order by foodId")
    public List<Food> getFoodByBusinessId(Integer businessId);

    /**
     * 根据foodId查询Food
     * @param foodId 食品ID
     * @return 食品
     */
    @Select("select * from food where foodId=#{foodId}")
    Food getFoodById(Integer foodId);
}
