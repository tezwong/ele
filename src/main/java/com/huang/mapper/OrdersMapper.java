package com.huang.mapper;

import com.huang.pojo.Orders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

import java.util.List;

@Mapper
public interface OrdersMapper {
    /**
     * 保存订单,设置userGeneratedKeys为true
     * @param orders
     * @return
     */
    @Insert("insert into orders values(null,#{userId},#{businessId},#{orderDate},#{orderTotal},#{daId},#{orderState})")
    @Options(useGeneratedKeys = true,keyProperty = "orderId",keyColumn = "orderId")
    int saveOrders(Orders orders);

    //按订单主键获取单体订单。
    Orders getOrdersById(Integer orderId);

    //按用户的ID获取订单集合
    public List<Orders> listOrdersByUserId(String userId);

}
