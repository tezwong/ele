package com.huang.mapper;

import com.huang.pojo.DeliveryAddress;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DeliveryAddressMapper {

    /**
     * 按用户的ID，查询所有快递地址
     * @param userId
     * @return
     */
    @Select("select * from deliveryAddress where userId=#{userId} order by daId")
    List<DeliveryAddress> listDeliveryAddressByUserId(String userId);

    /**
     * 按递送地址的主键查询
     * @param daId
     * @return
     */
    @Select("select * from deliveryAddress where daId=#{daId}")
    DeliveryAddress getDeliveryAddressById(Integer daId);

    /**
     * 保存递送地址
     * @param deliveryAddress
     * @return
     */
    @Insert("insert into deliveryAddress values(null,#{contactName},#{contactSex},#{contactTel},#{address},#{userId})")
    int saveDeliveryAddress(DeliveryAddress deliveryAddress);

    @Update("update deliveryAddress set contactName=#{contactName},contactSex=#{contactSex},contactTel=#{contactTel},address=#{address} where daid=#{daId}")
    int updateDeliveryAddress(DeliveryAddress deliveryAddress);

    @Delete("delete from deliveryAddress where daId=#{daId}")
    int removeDeliveryAddress(Integer daId);
}
