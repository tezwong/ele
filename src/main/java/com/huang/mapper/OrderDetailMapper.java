package com.huang.mapper;

import com.huang.pojo.OrderDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderDetailMapper {

    //1,批量保存订单
    int saveOrderDetailBatch(List<OrderDetail> list);

    //2,按订单编号，获取订单明细的集合
    List<OrderDetail> listOrderDetailByOrderId(Integer orderId);
}
