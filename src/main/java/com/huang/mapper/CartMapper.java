package com.huang.mapper;

import com.huang.pojo.Cart;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface CartMapper {
    /**
     * 保存购物车
     * @param cart
     * @return
     */
    @Insert("insert into cart values(null,#{foodId},#{businessId},#{userId},1)")
    int saveCart(Cart cart);

    /**
     * 修改购物车，只是修改购物车的数量，三个条件必须同时满足才可以修改
     * @param cart 需要修改的购物车
     * @return 整型，0或1
     */
    @Update("update cart set quantity=#{quantity} where foodId=#{foodId} and businessId=#{businessId} and userId=#{userId}")
    int updateCart(Cart cart);

    List<Cart> listCart(Cart cart);


    int removeCart(Cart cart);

}
