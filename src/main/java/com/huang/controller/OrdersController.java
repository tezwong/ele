package com.huang.controller;

import com.huang.pojo.Orders;
import com.huang.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @RequestMapping("/createOrders")
    public int createOrders(Orders orders) throws Exception {
        return ordersService.createOrders(orders);
    }

    @RequestMapping("/getordersbyid")
    public Orders getOrdersById(Integer orderId) throws Exception {
        return ordersService.getOrdersById(orderId);
    }

    @RequestMapping("/listOrders")
    public List<Orders> listOrdersByUserId(String userId) {
        return ordersService.listOrdersByUserId(userId);
    }


}
