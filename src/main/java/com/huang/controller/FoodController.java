package com.huang.controller;

import com.huang.pojo.Food;
import com.huang.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/food")
public class FoodController {

    @Autowired
    private FoodService foodService;

    @RequestMapping("/listFood")
    public List<Food> listFoodByBusinessId(Food food) {
        return foodService.listFoodByBusinessId(food.getBusinessId());
    }
}
