package com.huang.controller;

import com.huang.pojo.User;
import com.huang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/getUserByIdByPwd")
    public User getUserByIdByPwd(User user) throws Exception {
        return userService.getUserByIdByPwd(user);
    }

    @RequestMapping("/getUserById")
    public int getUserById(User user) throws Exception {
        return userService.getUserById(user.getUserId());
    }

    @RequestMapping("/saveUser")
    public int saveUser(User user) throws Exception {
        return userService.saveUser(user);
    }
}
