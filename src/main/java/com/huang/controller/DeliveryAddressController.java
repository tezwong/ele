package com.huang.controller;

import com.huang.pojo.DeliveryAddress;
import com.huang.service.DeliveryAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/address")
public class DeliveryAddressController {

    @Autowired
    private DeliveryAddressService deliveryAddressService;

    @RequestMapping("/listAddress")
    public List<DeliveryAddress> listDeliveryAddressByUserId(String userId) {
        return deliveryAddressService.listDeliveryAddressByUserId(userId);
    }

    @RequestMapping("/getAddressById")
    public DeliveryAddress getDeliveryAddressById(Integer daId) {
        return deliveryAddressService.getDeliveryAddressById(daId);
    }

    @RequestMapping("/saveAddress")
    public int saveDeliveryAddress(DeliveryAddress deliveryAddress) {
        return deliveryAddressService.saveDeliveryAddress(deliveryAddress);
    }

    @RequestMapping("/updateAddress")
    public int updateDeliveryAddress(DeliveryAddress deliveryAddress) {
        return deliveryAddressService.updateDeliveryAddress(deliveryAddress);
    }

    @RequestMapping("/removeAddress")
    public int removeDeliveryAddress(Integer daId) {
        return deliveryAddressService.removeDeliveryAddress(daId);
    }
}
