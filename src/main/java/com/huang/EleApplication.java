package com.huang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EleApplication {

    public static void main(String[] args) {
        SpringApplication.run(EleApplication.class, args);
    }

}
