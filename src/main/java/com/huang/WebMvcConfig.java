package com.huang;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author HuangZiy
 * @version 8.0
 * @date 2020/9/17 14:10
 */
@Configuration
public class WebMvcConfig {
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                //addCorsMappings 配置可以被跨域的路径，可以任意配置，也可以具体到直接的请求路径上
                //allowedMethods 允许跨域的请求方式
                //allowedOrigins 允许跨域访问的URL,可以是单条或者是多条内容
                //allowedHeaders 允许跨域的请求头header，可以自行设置请求头信息
                //maxAge 配置预检查请求的有效范围时间
                registry.addMapping("/**")
                        .allowedOrigins("http://localhost:8088")
                        .allowCredentials(true)
                        .allowedMethods("GET", "POST", "DELETE", "PUT", "PATCH")
                        .allowedHeaders("*")
                        .maxAge(36000);
            }
        };
    }
}
