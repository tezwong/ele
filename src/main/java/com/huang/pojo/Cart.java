package com.huang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author HuangZiy
 * @date 2020/09/15
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart implements Serializable {

    private Integer cartId;
    private Integer foodId;
    private Integer businessId;
    private String userId;
    private Integer quantity;

    private Food food;
    private Business business;
}
