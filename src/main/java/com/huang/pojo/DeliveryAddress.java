package com.huang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author HuangZiy
 * @date 2020/09/15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryAddress implements Serializable {
    private Integer daId;
    private String contactName;
    private Integer contactSex;
    private String contactTel;
    private String address;
    private String userId;
}
