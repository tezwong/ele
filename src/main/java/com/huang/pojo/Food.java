package com.huang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author HuangZiy
 * @date 2020/09/15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Food implements Serializable {

    private Integer foodId;
    private String foodName;
    private String foodExplain;
    private String foodImg;
    private Double foodPrice;
    private Integer businessId;
    private String remarks;

}
